function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}


window.onload = function(){
  document.getElementById("cookie-banner-ok").onclick = function()
  {
    document.getElementById("cookie-banner").className = "hidden";
    setCookie("cookie-banner", "ok", 10000);
    return false;
  }

  if (getCookie("cookie-banner") == ""){
    document.getElementById("cookie-banner").className = "";
  }
}
